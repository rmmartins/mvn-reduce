// Check this: http://bl.ocks.org/perrie/a567f84d4e8ed7ce5ce9
// Apparently you need HSL to use brighter()
function alpha(c, factor) {
  let color = d3.color(c, 1.0);
  color.r = color.r * factor + 255 * 1.0 * (1 - factor);
  color.g = color.g * factor + 255 * 1.0 * (1 - factor);
  color.b = color.b * factor + 255 * 1.0 * (1 - factor);
  return color;
}

function graphSort(a,b) {
  // Rule #1: selected vs. non-selected
  if (a.selected != b.selected) {
    return (a.selected | 0) - (b.selected | 0);
  }
  // Rule #2: node vs. edge
  if (("label" in a) != ("label" in b)) {
    return (("label" in a) | 0) - (("label" in b) | 0)
  }
  // Rule #3: stable ordering
  return a.index - b.index;
}