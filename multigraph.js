
function drawMultiGraph(graph, brush_callback) {

  // Compute correct width and height
  const main = d3.select('#main').node();
  const styles = window.getComputedStyle(main);
  const padding = parseFloat(styles.paddingLeft) + parseFloat(styles.paddingRight);
  const big_side = main.clientWidth - padding;

  // Pre-defined color scales for each type of scalar
  let colorScales = [];
  colorScales['lin'] = d3.scaleSequential(d3.interpolateViridis);
  colorScales['div'] = d3.scaleLinear().range(['#00f', '#fff', '#f00']);
  colorScales['cat'] = d3.scaleOrdinal(d3.schemeCategory10);
  colorScales['non'] = d3.scaleLinear().domain([0,1]).range(['lightgray', 'lightgray']);
  let colorType = 'lin';
  let colorScale = colorScales[colorType];
  let colorIndex = 0;

  // Size scale is always linear
  let maxRadius = 20;
  let sizeScale = d3.scaleLinear().domain([0,1]).range([5, maxRadius]);
  let sizeIndex = 0;

  //  Global X and Y colorScales so that all plots are comparable
  let xMin = d3.min(graph['projs'], p => d3.min(p, c => c[0]));
  let xMax = d3.max(graph['projs'], p => d3.max(p, c => c[0]));
  let yMin = d3.min(graph['projs'], p => d3.min(p, c => c[1]));
  let yMax = d3.max(graph['projs'], p => d3.max(p, c => c[1]));
  let domain = [d3.min([xMin, yMin]), d3.max([xMax, yMax])];
  const xTrans = (domain[0] + domain[1]) / 2 - (xMin + xMax) / 2;
  const yTrans = (domain[0] + domain[1]) / 2 - (yMin + yMax) / 2;

  const margin = maxRadius * 2;

  let _x = d3.scaleLinear()
      .domain(domain.map(x => x - xTrans))
      // .domain(domain)
      .range([margin, big_side - margin]);

  let _y = d3.scaleLinear()
      .domain(domain.map(y => y - yTrans))
      // .domain(domain)
      .range([big_side - margin, margin]);

  let svg = d3.select('#main').append('svg')
      .attr('width', big_side)
      .attr('height', big_side)
      .style('position', 'relative');

  // Edges are currently based on index :(
  let labelFor = graph['vertices'].reduce((accum, v) => {
    accum[v.index] = v.label;
    return accum;
  }, {});

  // Everything related to selection/brushing:

  // TODO remove global sel and do it right
  const brush = d3.brush()
      .on("brush end.1", function() {
        if (!d3.event.sourceEvent.isTrusted) return;
        let s = d3.event.selection;
        global_sel = [];
        if (s) {
          // TODO do a quadtree search
          g.selectAll('.node').each(function(d) {
            if (+this.getAttribute('cx') > s[0][0] && +this.getAttribute('cx') < s[1][0] &&
                +this.getAttribute('cy') > s[0][1] && +this.getAttribute('cy') < s[1][1]) {
              global_sel.push(this);
            }
          });
        }
      })
      .on("end.2", function() {
        if (!d3.event.sourceEvent.isTrusted) return;
        brushSel.call(brush.move, null);
      })
      .on("start.cb end.cb brush.cb", brush_callback);

  let brushSel = svg.append("g")
    .attr("class", "brush")
    // .attr('transform', `translate(${margin*2},${margin*2})`)
    .call(brush);

  let g = svg.append('g');
      // .attr('transform', `translate(${margin*2},${margin*2})`);

  // This adds an index to each edge, to facilitate ordering later
  for (let i = 0; i < graph['edges'].length; ++i) {
    graph['edges'][i].index = i;
  }

  // Draw edges first
  g.selectAll('.edge')
      .data(graph['edges'])
    .enter().append('line')
      .attr('class', 'el edge');

  g.selectAll('.node')
      .data(graph['vertices'])
    .enter().append('circle')
      .attr('id', d => d.label)
      .attr('class', 'el node');

  let len = graph['projs'].length;
  let weight = Math.floor(len/2);

  // Setup the combo boxes

  let attrs = ['None'];
  for (let scalar of graph.scalars) {
    attrs.push(scalar[0]);
  }

  function addOptions(select) {
    let options = select.selectAll('option')
          .data(attrs);

    options.exit().remove();

    options.enter().append('option')
        .merge(options)
          .attr('value', (d,i) => i)
          .text(d => d);
  }

  var legend = Legend(svg);

  d3.select('#color')
      .call(addOptions)
      .on('change', function() {
        colorIndex = d3.select('#color').property('selectedIndex');
        // scalarIndex == 0 means 'None'
        if (colorIndex > 0) {
          colorType = graph.scalars[colorIndex - 1][1];
          colorScale = colorScales[colorType]
          switch (colorType) {
            case 'cat':
              colorScale.domain([...new Set(graph.vertices.map(d => d.scalars[colorIndex - 1]))]);
              break;
            case 'div':
              let domain = d3.extent(graph.vertices, d => d.scalars[colorIndex - 1]);
              colorScale.domain([domain[0], 0, domain[1]]);
              break;
            case 'lin':
              colorScale.domain(d3.extent(graph.vertices, d => d.scalars[colorIndex - 1]));
              break;
          }
        } else {
          colorType = 'non';
          colorScale = colorScales[colorType]
        }
        legend();
        update();
      });

  d3.select('#size')
      .call(addOptions)
      .on('change', function() {
        sizeIndex = d3.select('#size').property('selectedIndex');
        // scalarIndex == 0 means 'None'
        if (sizeIndex > 0) {
          sizeScale.domain(d3.extent(graph.vertices, v => v.scalars[sizeIndex - 1]));
        } else {
          sizeScale.domain([0,1]);
        }
        update();
      });

  function update(w) {
    // TODO Many things in this function would work better if I just tagged selected nodes
    //      with a specific 'selected' class.
    if (w) {
      weight = Math.floor(w);
    }

    let proj = graph['projs'][weight];

    // Screen-space interpolation (if necessary)
    if (w && weight != w) {
      let step = w - weight;
      let p0 = graph['projs'][weight];
      let p1 = graph['projs'][weight + 1];
      proj = [];
      for (let i = 0; i < p0.length; ++i) {
        proj.push([
          p0[i][0] * (1.0 - step) + p1[i][0] * step,
          p0[i][1] * (1.0 - step) + p1[i][1] * step
          ]);
      }
    }

    let selIds = global_sel.map(d => d.__data__.label);

    // Draw edges first
    g.selectAll('.edge')
        .attrs(d => {
          let s = d['source'];
          let t = d['target'];
          return {
            'x1': _x(proj[s][0]), 'x2': _x(proj[t][0]),
            'y1': _y(proj[s][1]), 'y2': _y(proj[t][1])
          }
        })
        .style('opacity', 0.3)
        .each(function(d) {
          d.selected = false;
          let source = graph['vertices'][d.source].label;
          let target = graph['vertices'][d.target].label;
          if (selIds.length > 0) {
            if (selIds.includes(source) && selIds.includes(target)) {
              d.selected = true;
              d3.select(this)
                  .style('opacity', 1);
            } else {
              d3.select(this).style('opacity', 0.1);
            }
          }
        });

    g.selectAll('.node')
        .attr('fill', d => colorScale(d.scalars[colorIndex - 1] || 0))
        .attr('r', d => sizeScale(d.scalars[sizeIndex - 1] || 0))
        .attr('cx', d => _x(proj[d['index']][0]) + xTrans)
        .attr('cy', d => _y(proj[d['index']][1]))
        .style('stroke-width', 1)
        .style('stroke', 'black')
        .on('mouseenter', function(d) {
          let x1 = _x(proj[d['index']][0]);
          let y1 = _y(proj[d['index']][1]);
          if (graph.scalars[0][0].startsWith('Sample')) {
            drawPopup(g, x1, y1, d);
          }
        })
        .on('mouseout', function(d) {
          d3.selectAll('.popup').remove();
        })
        // TODO This can be improved...?
        .each(function(d) {
          d.selected = false;
          let d3this = d3.select(this);
          if (global_sel.length > 0) {
            if (global_sel.includes(this)) {
              d.selected = true;
              d3this.style('stroke', 'black');
              d3this.style('stroke-width', 2);
            } else {
              let color = d3this.attr('fill');
              d3this.attr('fill', alpha(color, 0.1));
              d3this.style('stroke', alpha('black', 0.1));
              d3this.style('stroke-width', 1);
            }
          }
        });

    g.selectAll('.el').sort(graphSort);

  }

  function drawPopup(sel, x, y, datum) {
    // space to upper-right border
    let dx = svg.attr('width') - x;
    let dy = y;
    // if it's not enough, mirror
    let x2 = (dx < 70 ? x - 10 : x + 10);
    let x3 = (dx < 70 ? x - 60 : x + 60);
    let y2 = (dy < 70 ? y + 10 : y - 10);
    let y3 = (dy < 70 ? y2 + 3 : y2 - 37);
    let yr = (dy < 70 ? y + 10 : y - 50);
    let xr = (dx < 70 ? x - 60 : x + 10);
    let xt = (dx < 70 ? x - 35 : x + 35);
    let yt = (dy < 70 ? y + 60 : y);

    sel.append('line')
        .attr('class', 'popup')
        .attrs(() => {
          return {
            'x1': x, 'y1': y, 'x2': x2, 'y2': y2
          }
        });
    sel.append('rect')
        .attr('class', 'popup')
        .attrs(() => {
          return {
            'x': xr, 'y': yr, 'width': 50, 'height': 60,
            'fill': 'lightyellow', 'stroke': 'black'
          }
        })
    sel.append('text')
      .attr('class', 'popup')
      .attrs(() => {
        return {
          'x': xt, 'y': yt, 'fill': 'black'
        }
      })
      .text(datum['label']);
    // draw a mini bar chart
    let xScale = d3.scaleBand()
        .domain(d3.range(4))
        .range([x2, x3])
        .paddingInner([0.2])
        .paddingOuter([0.2]);
    sel.selectAll('.bar')
        .data(datum['scalars'])
      .enter().append('rect')
        .attr('class', 'popup')
        .attr('fill', d => colorScale(d))
        .attr('x', (d,i) => xScale(i))
        .attr('y', y3)
        .attr('width', xScale.bandwidth())
        .attr('height', 34);
  }

  function Legend(svg) {
    let defs = svg.append('defs');
    let grad = defs.append("linearGradient")
        .attr("id", "legend-grad");

    let g = svg.append('g')
        .attr('class', 'legend')
        .attr('transform', 'translate(15,5)');

    let width = 150;

    return function() {
      let decimal = 2;
      let domain = colorScale.domain();

      if (colorType == 'cat') {
        decimal = 0;
      }

      if (colorType == 'lin') {
        let step = (domain[1] - domain[0]) / 4;
        domain = d3.range(domain[0], domain[1] + step, step);
      }

      if (colorType == 'non') {
        g.attr('visibility', 'hidden');
        return;
      } else {
        g.attr('visibility', 'visible');
      }

      // Labels
      let labels = g.selectAll('text')
        .data(domain);

      labels.exit().remove();

      // Gradient or Bars
      g.select('.border').remove();

      if (colorType == 'cat') {
        let bands = d3.scaleBand()
            .domain(domain)
            .range([0, width]);

        labels.enter().append('text')
          .attr('class', 'label')
        .merge(labels)
          .attr('x', d => bands(d) + bands.step() / 2)
          .attr('y', 17)
          .text(d => d.toFixed(decimal));

        let rect = g.selectAll('rect')
          .data(domain);

        rect.exit().remove();

        rect.enter().append('rect')
          .merge(rect)
            .attr('class', 'grad')
            .attr('x', bands)
            .attr('y', 0)
            .attr('width', bands.step())
            .attr('height', 15)
            .attr('stroke', 'none')
            .attr('fill', colorScale);
      } else {
        labels.enter().append('text')
          .attr('class', 'label')
        .merge(labels)
          .attr('x', (d,i) => i * width / (domain.length - 1))
          .attr('y', 17)
          .text(d => d.toFixed(decimal));

        let rect = g.selectAll('rect')
          .data([0]);

        rect.exit().remove();

        rect.enter().append('rect')
          .merge(rect)
            .attr('class', 'grad')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', width)
            .attr('height', 15)
            .attr('fill', 'url(#legend-grad)');

        let stops = grad.selectAll('stop')
            .data(domain);

        stops.exit().remove();

        stops.enter().append("stop")
          .merge(stops)
            .attr("offset", (d,i) => i / (domain.length - 1))
            .attr("stop-color", d => colorScale(d));
      }

      g.append('rect')
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', width)
          .attr('height', 15)
          .attr('class', 'border')
          .attr('fill', 'none')
          .attr('stroke', 'black');
    }
  }

  if (graph.scalars[0][0].startsWith('Sample')) {
    d3.select('#color').property('value', '4').dispatch('change');
    d3.select('#size').property('value', '3').dispatch('change');
  } else {
    d3.select('#color').property('value', '2').dispatch('change');
    d3.select('#size').property('value', '1').dispatch('change');
  }

  return update;
}

