function draw(graph, index, col) {
  const colId = '#' + col;
  const side = $(colId).width();
  const margin = 10;

  let svg = d3.select(colId).append('svg')
    .attr('height', side);

  let g = svg.append('g')
      .attr('transform', `translate(${margin},${margin})`);

  // The current projection coordinates
  let proj = graph['projs'][index];

  //  Same domain for both axes
  let xExt = d3.extent(proj, p => p[0]);
  let yExt = d3.extent(proj, p => p[1]);
  let domain = [d3.min([xExt[0], yExt[0]]), d3.max([xExt[1], yExt[1]])];
  const xTrans = (domain[0] + domain[1]) / 2 - (xExt[0] + xExt[1]) / 2;
  const yTrans = (domain[0] + domain[1]) / 2 - (yExt[0] + yExt[1]) / 2;

  let x = d3.scaleLinear()
      .domain(domain.map(x => x - xTrans))
      // .domain(domain)
      .range([0, side - 2 * margin]);

  let y = d3.scaleLinear()
      .domain(domain.map(y => y - yTrans))
      // .domain(domain)
      .range([side - 2 * margin, 0]);

  // Draw edges first
  g.selectAll('.edge')
      .data(graph['edges'])
    .enter().append('line')
      .attr('class', 'el edge')
      .attrs(d => {
        let s = d['source'];
        let t = d['target'];
        return {
          'x1': x(proj[s][0]), 'x2': x(proj[t][0]),
          'y1': y(proj[s][1]), 'y2': y(proj[t][1])
        }
      })
      .style('opacity', 0.25);

  g.selectAll('.node')
      .data(graph['vertices'])
    .enter().append('circle')
      .attr('id', d => d.label)
      .attr('class', 'el node')
      .attr('cx', d => x(proj[d['index']][0]))
      .attr('cy', d => y(proj[d['index']][1]))
      .attr('r', 3)
      .attr('fill', 'gray');
      // .on('mouseover', d => {
      //   console.log(d['label'])
      // });

  function update() {
    // let sel = global_sel.reduce((a,d) => a + '#' + d.getAttribute('id') + ',', '').slice(0, -1);
    let selIds = global_sel.map(d => d.getAttribute('id'));
    let nodeFill = 'gray', nodeStroke = 'black';

    // Draw edges first
    g.selectAll('.edge')
        .style('opacity', 0.3)
        .each(function(d) {
          let source = graph['vertices'][d.source].label;
          let target = graph['vertices'][d.target].label;
          if (selIds.length > 0) {
            if (selIds.includes(source) && selIds.includes(target)) {
              d3.select(this).style('opacity', 1);
            } else {
              d3.select(this).style('opacity', 0.1);
            }
          }
        });

    g.selectAll('.node')
        .each(function(d) {
            let d3this = d3.select(this);
            if (selIds.length > 0) {
              if (selIds.includes(this.getAttribute('id'))) {
                d3this.attr('fill', nodeFill);
                d3this.style('stroke', nodeStroke);
                d3this.style('stroke-width', 1);
              }
              else {
                d3this.attr('fill', alpha(nodeFill, 0.1));
                d3this.style('stroke', alpha(nodeStroke, 0.1));
                d3this.style('stroke-width', 1);
              }
            }
            else {
                d3this.attr('fill', nodeFill);
                d3this.style('stroke', nodeStroke);
                d3this.style('stroke-width', 1);
            }
          });

    g.selectAll('.el').sort(graphSort);

  }

  update.scale = function() {
      return [x,y];
  }

  return update;
}
